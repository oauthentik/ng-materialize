import { Component, ViewChild } from '@angular/core';
import { SelectDirective } from './select.directive';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';
  @ViewChild(SelectDirective) private select: SelectDirective;
  dropdownValue:string;
  items = [
    {
      "text": "American Samoa",
      "value": "Indiana"
    },
    {
      "text": "Oregon",
      "value": "Idaho"
    },
    {
      "text": "Iowa",
      "value": "Alabama"
    },
    {
      "text": "Montana",
      "value": "Illinois"
    },
    {
      "text": "Florida",
      "value": "Marshall Islands"
    },
    {
      "text": "Georgia",
      "value": "Maryland"
    },
    {
      "text": "Colorado",
      "value": "Ohio"
    }
  ]
  changeOptions(e){
    let value =  e.target.value;
    this.select.options.first.value = value
    this.select.options.first.text = value
    this.select.options.setDirty()

  }
  selectionChanged(item){
    console.log(item);

  }
  printSelection(selection){
    this.dropdownValue = selection;
  }
}
